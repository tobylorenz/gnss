/*
 * Copyright (C) 2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <GNSS/Position.h>

namespace GNSS {

Position::Position() :
    latitude(),
    longitude(),
    altitude(),
    speed(),
    course()
{
}

#ifdef LIBGPS_FOUND

Position::Position(const gps_fix_t & gpsFix) :
    latitude(gpsFix.latitude),
    longitude(gpsFix.longitude),
    altitude(gpsFix.altitude),
    speed(gpsFix.speed * 60 / 1000), // m/s -> km/h
    course(gpsFix.track)
{
}

Position & Position::operator=(const gps_fix_t & gpsFix)
{
    latitude = gpsFix.latitude;
    longitude = gpsFix.longitude;
    altitude = gpsFix.altitude;
    speed = gpsFix.speed * 60 / 1000; // m/s -> km/h
    course = gpsFix.track;
    return *this;
}

Position::operator gps_fix_t() const
{
    gps_fix_t gpsFix;
    // time
    // mode
    // ept
    gpsFix.latitude = latitude;
    // epy
    gpsFix.longitude = longitude;
    // epx
    gpsFix.altitude = altitude;
    // epv
    gpsFix.track = course;
    // epd
    gpsFix.speed = speed / 60 * 1000; // km/h -> m/s
    // eps
    // climb
    // epc
    return gpsFix;
}

#endif

#ifdef VECTOR_ASC_FOUND

Position::Position(const Vector::ASC::Gps & gps) :
    latitude(gps.latitude),
    longitude(gps.longitude),
    altitude(gps.altitude),
    speed(gps.speed),
    course(gps.course)
{
}

Position & Position::operator=(const Vector::ASC::Gps & gps)
{
    latitude = gps.latitude;
    longitude = gps.longitude;
    altitude = gps.altitude;
    speed = gps.speed;
    course = gps.course;
    return *this;
}

Position::operator Vector::ASC::Gps() const
{
    Vector::ASC::Gps gps;
    // time
    // channel
    gps.latitude = latitude;
    gps.longitude = longitude;
    gps.altitude = altitude;
    gps.speed = speed;
    gps.course = course;
    return gps;
}

#endif

#ifdef VECTOR_BLF_FOUND

Position::Position(const Vector::BLF::GpsEvent & gpsEvent) :
    latitude(gpsEvent.latitude),
    longitude(gpsEvent.longitude),
    altitude(gpsEvent.altitude),
    speed(gpsEvent.speed),
    course(gpsEvent.course)
{
}

Position & Position::operator=(const Vector::BLF::GpsEvent & gpsEvent)
{
    latitude = gpsEvent.latitude;
    longitude = gpsEvent.longitude;
    altitude = gpsEvent.altitude;
    speed = gpsEvent.speed;
    course = gpsEvent.course;
    return *this;
}

Position::operator Vector::BLF::GpsEvent() const
{
    Vector::BLF::GpsEvent gpsEvent;
    // ObjectHeaderBase
    // ObjectHeader
    // channel
    gpsEvent.latitude = latitude;
    gpsEvent.longitude = longitude;
    gpsEvent.altitude = altitude;
    gpsEvent.speed = speed;
    gpsEvent.course = course;
    return gpsEvent;
}

#endif

}
