# dependencies
include(GenerateExportHeader)

# targets
add_library(GNSS SHARED "")

# search paths
include_directories(
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_BINARY_DIR}/src)

# sources/headers
target_sources(GNSS
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/platform.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Position.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/Position.cpp)

# generated files
configure_file(config.h.in config.h)
configure_file(GNSS.pc.in GNSS.pc @ONLY)
generate_export_header(GNSS)

# compiler/linker settings
set_target_properties(GNSS PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
    CXX_VISIBILITY_PRESET "hidden"
    SOVERSION ${PROJECT_VERSION_MAJOR}
    VERSION ${PROJECT_VERSION}
    VISIBILITY_INLINES_HIDDEN 1)
if(CMAKE_COMPILER_IS_GNUCXX)
    add_definitions(-pedantic)
    add_definitions(-D_FORTIFY_SOURCE=2)
    #add_definitions(-fPIE -pie)
    add_definitions(-Wl,-z,relro,-z,now)
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 4.9)
        add_definitions(-fstack-protector-strong)
    endif()
    if(OPTION_USE_GCOV)
        add_definitions(-g -O0 --coverage)
    endif()
    if(OPTION_USE_GPROF)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
        set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
    endif()
endif()
if(VECTOR_ASC_FOUND)
    target_link_libraries(GNSS ${VECTOR_ASC_LIBRARIES})
endif()
if(VECTOR_BLF_FOUND)
    target_link_libraries(GNSS ${VECTOR_BLF_LIBRARIES})
endif()
if(OPTION_USE_GCOV)
    target_link_libraries(GNSS gcov)
endif()

# install
install(
    TARGETS GNSS
    DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/GNSS.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
install(
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/gnss_export.h
        ${CMAKE_CURRENT_BINARY_DIR}/config.h
        $<TARGET_PROPERTY:GNSS,INTERFACE_SOURCES>
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/GNSS)

# sub directories
add_subdirectory(docs)
add_subdirectory(tests)
