/*
 * Copyright (C) 2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Position
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <GNSS.h>

#ifdef LIBGPS_FOUND
#include <gps.h>
#endif

#ifdef VECTOR_ASC_FOUND
#include <Vector/ASC/Gps.h>
#endif

#ifdef VECTOR_BLF_FOUND
#include <Vector/BLF/GpsEvent.h>
#endif

#ifdef LIBGPS_FOUND

/**
 * Convert from gps_fix_t to GNSS::Position.
 */
BOOST_AUTO_TEST_CASE(fromLibgps)
{
    gps_fix_t gpsFix;
    gpsFix.latitude = 1.0;
    gpsFix.longitude = 2.0;
    gpsFix.altitude = 3.0;
    gpsFix.track = 4.0;
    gpsFix.speed = 5.0;

    /* constructor */
    GNSS::Position position1(gpsFix);
    BOOST_CHECK_EQUAL(gpsFix.latitude, position1.latitude);
    BOOST_CHECK_EQUAL(gpsFix.longitude, position1.longitude);
    BOOST_CHECK_EQUAL(gpsFix.altitude, position1.altitude);
    BOOST_CHECK_EQUAL(gpsFix.track, position1.course);
    BOOST_CHECK_EQUAL(gpsFix.speed, position1.speed);

    /* assignment */
    GNSS::Position position2;
    position2 = gpsFix;
    BOOST_CHECK_EQUAL(gpsFix.latitude, position2.latitude);
    BOOST_CHECK_EQUAL(gpsFix.longitude, position2.longitude);
    BOOST_CHECK_EQUAL(gpsFix.altitude, position2.altitude);
    BOOST_CHECK_EQUAL(gpsFix.track, position2.course);
    BOOST_CHECK_EQUAL(gpsFix.speed, position2.speed);
}

/**
 * Convert from GNSS::Position to gps_fix_t.
 */
BOOST_AUTO_TEST_CASE(toLibgps)
{
    GNSS::Position position;
    position.latitude = 1.0;
    position.longitude = 2.0;
    position.altitude = 3.0;
    position.speed = 4.0;
    position.course = 5.0;

    /* assignment */
    gps_fix_t gpsFix;
    gpsFix = position;
    BOOST_CHECK_EQUAL(position.latitude, gpsFix.latitude);
    BOOST_CHECK_EQUAL(position.longitude, gpsFix.longitude);
    BOOST_CHECK_EQUAL(position.altitude, gpsFix.altitude);
    BOOST_CHECK_EQUAL(position.speed, gpsFix.speed);
    BOOST_CHECK_EQUAL(position.course, gpsFix.track);
}

#endif

#ifdef VECTOR_ASC_FOUND

/**
 * Convert from Vector::ASC::Gps to GNSS::Position.
 */
BOOST_AUTO_TEST_CASE(fromVectorASC)
{
    Vector::ASC::Gps gps;
    gps.latitude = 1.0;
    gps.longitude = 2.0;
    gps.altitude = 3.0;
    gps.speed = 4.0;
    gps.course = 5.0;

    /* constructor */
    GNSS::Position position1(gps);
    BOOST_CHECK_EQUAL(gps.latitude, position1.latitude);
    BOOST_CHECK_EQUAL(gps.longitude, position1.longitude);
    BOOST_CHECK_EQUAL(gps.altitude, position1.altitude);
    BOOST_CHECK_EQUAL(gps.speed, position1.speed);
    BOOST_CHECK_EQUAL(gps.course, position1.course);

    /* assignment */
    GNSS::Position position2;
    position2 = gps;
    BOOST_CHECK_EQUAL(gps.latitude, position2.latitude);
    BOOST_CHECK_EQUAL(gps.longitude, position2.longitude);
    BOOST_CHECK_EQUAL(gps.altitude, position2.altitude);
    BOOST_CHECK_EQUAL(gps.speed, position2.speed);
    BOOST_CHECK_EQUAL(gps.course, position2.course);
}

/**
 * Convert from GNSS::Position to Vector::ASC::Gps.
 */
BOOST_AUTO_TEST_CASE(toVectorASC)
{
    GNSS::Position position;
    position.latitude = 1.0;
    position.longitude = 2.0;
    position.altitude = 3.0;
    position.speed = 4.0;
    position.course = 5.0;

    /* assignment */
    Vector::ASC::Gps gps;
    gps = position;
    BOOST_CHECK_EQUAL(position.latitude, gps.latitude);
    BOOST_CHECK_EQUAL(position.longitude, gps.longitude);
    BOOST_CHECK_EQUAL(position.altitude, gps.altitude);
    BOOST_CHECK_EQUAL(position.speed, gps.speed);
    BOOST_CHECK_EQUAL(position.course, gps.course);
}

#endif

#ifdef VECTOR_BLF_FOUND

/**
 * Convert from Vector::BLF::GpsEvent to GNSS::Position.
 */
BOOST_AUTO_TEST_CASE(fromVectorBLF)
{
    Vector::BLF::GpsEvent gpsEvent;
    gpsEvent.latitude = 1.0;
    gpsEvent.longitude = 2.0;
    gpsEvent.altitude = 3.0;
    gpsEvent.speed = 4.0;
    gpsEvent.course = 5.0;

    /* constructor */
    GNSS::Position position1(gpsEvent);
    BOOST_CHECK_EQUAL(gpsEvent.latitude, position1.latitude);
    BOOST_CHECK_EQUAL(gpsEvent.longitude, position1.longitude);
    BOOST_CHECK_EQUAL(gpsEvent.altitude, position1.altitude);
    BOOST_CHECK_EQUAL(gpsEvent.speed, position1.speed);
    BOOST_CHECK_EQUAL(gpsEvent.course, position1.course);

    /* assignment */
    GNSS::Position position2;
    position2 = gpsEvent;
    BOOST_CHECK_EQUAL(gpsEvent.latitude, position2.latitude);
    BOOST_CHECK_EQUAL(gpsEvent.longitude, position2.longitude);
    BOOST_CHECK_EQUAL(gpsEvent.altitude, position2.altitude);
    BOOST_CHECK_EQUAL(gpsEvent.speed, position2.speed);
    BOOST_CHECK_EQUAL(gpsEvent.course, position2.course);
}

/**
 * Convert from GNSS::Position to Vector::BLF::GpsEvent.
 */
BOOST_AUTO_TEST_CASE(toVectorBLF)
{
    GNSS::Position position;
    position.latitude = 1.0;
    position.longitude = 2.0;
    position.altitude = 3.0;
    position.speed = 4.0;
    position.course = 5.0;

    /* assignment */
    Vector::BLF::GpsEvent gpsEvent;
    gpsEvent = position;
    BOOST_CHECK_EQUAL(position.latitude, gpsEvent.latitude);
    BOOST_CHECK_EQUAL(position.longitude, gpsEvent.longitude);
    BOOST_CHECK_EQUAL(position.altitude, gpsEvent.altitude);
    BOOST_CHECK_EQUAL(position.speed, gpsEvent.speed);
    BOOST_CHECK_EQUAL(position.course, gpsEvent.course);
}

#endif
