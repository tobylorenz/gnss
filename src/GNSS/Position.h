/*
 * Copyright (C) 2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <GNSS/config.h>
#include <GNSS/platform.h>

#include <cstdint>
#include <string>

#ifdef LIBGPS_FOUND
#include <gps.h>
#endif

#ifdef VECTOR_ASC_FOUND
#include <Vector/ASC/Gps.h>
#endif

#ifdef VECTOR_BLF_FOUND
#include <Vector/BLF/GpsEvent.h>
#endif

#include <GNSS/gnss_export.h>

namespace GNSS {

/** Position */
class GNSS_EXPORT Position
{
public:
    Position();

    /**
     * Latitude, possible values reach from -180 to 180.
     *
     * Negative values are western hemisphere, positive
     * values are eastern hemisphere.
     */
    double latitude;

    /**
     * Longitude, possible values reach from -90 to 90.
     *
     * Negative values are Southern hemisphere,
     * positive values are northern hemisphere.
     */
    double longitude;

    /** Altitude in meters, measured above sea line. */
    double altitude;

    /** Current vehicle speed in km/h. */
    double speed;

    /**
     * Current driving course, possible values reach
     * from -180 to 180. A value of 0 means driving
     * north, 90 means driving east, -90 means driving
     * west, -180 and 180 mean driving south.
     */
    double course;

#ifdef LIBGPS_FOUND
    Position(const gps_fix_t & gpsFix);
    Position & operator=(const gps_fix_t & gpsFix);
    operator gps_fix_t() const;
#endif

#ifdef VECTOR_ASC_FOUND
    Position(const Vector::ASC::Gps & gps);
    Position & operator=(const Vector::ASC::Gps & gps);
    operator Vector::ASC::Gps() const;
#endif

#ifdef VECTOR_BLF_FOUND
    Position(const Vector::BLF::GpsEvent & gpsEvent);
    Position & operator=(const Vector::BLF::GpsEvent & gpsEvent);
    operator Vector::BLF::GpsEvent() const;
#endif
};

}
