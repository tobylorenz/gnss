# Introduction

This is a library to abstract GNSS (Global Navigation Satellite System)
services, including

  - NAVSTAR GPS (United States of America)
  - GLONASS (Russia)
  - Galileo (European Union)
  - Beidou (China)

It supports position conversions between the following libraries:

  - libgps
  - Vector::ASC
  - Vector::BLF

# Build

Create a build directory and run cmake therein, e.g.

    mkdir <build-dir>
    cd <build-dir>
    cmake <source-dir>
    cmake --build .

# Test

Static tests are

* Cppcheck (if OPTION_RUN_CPPCHECK is set)
* CCCC (if OPTION_RUN_CCCC is set)

Dynamic tests are

* Unit tests (if OPTION_BUILD_TESTS is set)
* Example runs (if OPTION_BUILD_EXAMPLES is set)
* Coverage (if OPTION_USE_GCOV is set)
* Coverage Report (if OPTION_ADD_LCOV is set)

The test execution can be triggered using

    ctest

# Package

The package generation can be triggered using

    cpack

# Repository Structure

The following files are part of the source code distribution:

* src/_project_/
* src/_project_/tests/

The following files are working directories for building and testing:

* build/_project_/

The following files are products of installation and building:

* bin/
* lib/
* share/doc/_project_/
* share/man/
* include/_project_/
